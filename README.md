<p align="center">
    <img src="https://be-to.nl/images/round.png" alt="BeTo Logo" style="background-color: transparent"/>
</p>

# Theme Customizations for BeTo Laravel

![Version](https://img.shields.io/packagist/v/beto/laravel-elastic-app-search?style=for-the-badge&label=Version&link=https%3A%2F%2Fpackagist.org%2Fpackages%2Fbeto%2Fthemes)
![BeTo Laravel Version](https://img.shields.io/packagist/dependency-v/beto/laravel-elastic-app-search/beto%2Flaravel?style=for-the-badge&label=BeTo&link=https%3A%2F%2Fgitlab.com%2Fberkvenstoonen%2Flaravel%2Fplugins%2Fbase)
![Laravel Version](https://img.shields.io/packagist/dependency-v/beto/laravel-elastic-app-search/laravel%2Fframework?style=for-the-badge&label=Laravel&link=https%3A%2F%2Flaravel.com%2F)
![Contributions Welcome](https://img.shields.io/badge/contributions-welcome-green.svg?style=for-the-badge&link=https%3A%2F%2Fgitlab.com%2Fberkvenstoonen%2Flaravel%2Fplugins%2Fthemes)
