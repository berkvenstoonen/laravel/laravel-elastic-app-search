<?php
declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\Services;

use BeTo\Laravel\Exceptions\DependencyException;
use BeTo\LaravelElasticAppSearch\Interfaces\AppSearchEntityInterface;
use HaydenPierce\ClassFinder\ClassFinder;
use Webmozart\Assert\Assert;

final class GetAppSearchModels
{
    /**
     * @return array<class-string>
     * @throws DependencyException
     */
    public static function getClasses(): array
    {
        try {
            $namespace = config('elastic-app-search.model_namespace');
            Assert::string($namespace);
            return array_filter(ClassFinder::getClassesInNamespace($namespace, ClassFinder::RECURSIVE_MODE), fn(string $className) => is_a($className, AppSearchEntityInterface::class, true));
        } catch (\Exception $exception) {
            throw new DependencyException($exception);
        }
    }

    /**
     * @return \Generator<AppSearchEntityInterface>
     * @throws DependencyException
     */
    public static function getEntities(): \Generator
    {
        foreach (self::getClasses() as $className) {
            if (is_a($className, AppSearchEntityInterface::class, true)) {
                foreach ($className::getIndexableEntities() as $entity) {
                    yield $entity;
                }
            }
        }
    }

    /**
     * @return \Generator<string>
     * @throws DependencyException
     */
    public static function getEngineNames(): \Generator
    {
        foreach (self::getClasses() as $className) {
            if (is_a($className, AppSearchEntityInterface::class, true)) {
                yield $className::getEngineName();
            }
        }
    }
}
