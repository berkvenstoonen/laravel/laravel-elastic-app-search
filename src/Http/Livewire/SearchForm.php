<?php
declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\Http\Livewire;

use BeTo\LaravelElasticAppSearch\ElasticSearch\ElasticSearchClient;
use Illuminate\View\View;
use Livewire\Component;

class SearchForm extends Component
{
    public string $route;
    /** @var array<int> */
    public array $tags = [];
    /** @var array<string|int> */
    public array $autocompleteFilters = [];
    /** @var array<string|int> */
    public array $search = [];
    /** @var array<string|int> */
    public array $selected = [];

    /**
     * @param array<int> $tags
     */
    public function mount(array $tags, string $route, array $autocompleteFilters = []): void
    {
        $this->tags                = $tags;
        $this->route               = $route;
        $this->autocompleteFilters = $autocompleteFilters;
    }

    /**
     * @param array<string|int> $search
     */
    public function updatedSearch(array $search): void
    {
        $this->dispatch('search-form-updated', $search);
    }

    public function render(): View
    {
        $autocompleteUrl     = env('ELASTIC_APP_SEARCH_SERVER') . '/api/as/v1/engines/' . ElasticSearchClient::getEngineName('autocomplete') . '/search.json';
        $autocompleteHeaders = ['Authorization' => 'Bearer ' . env('ELASTIC_APP_SEARCH_SEARCH_TOKEN'), 'Content-Type' => 'application/json'];
        return view('beto::livewire.search-form', compact('autocompleteUrl', 'autocompleteHeaders'));
    }
}
