<?php
declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\Http\Livewire;

use BeTo\LaravelElasticAppSearch\ElasticSearch\SearchResults;
use Illuminate\Support\Collection;

abstract class LoadMoreBlock extends \BeTo\Laravel\Http\Livewire\LoadMoreBlock
{
    protected SearchResults $searchResults;

    abstract protected function performSearch(): SearchResults;

    protected function getSearchResults(): SearchResults
    {
        if (!isset($this->searchResults)) {
            $this->searchResults = $this->performSearch();
        }
        return $this->searchResults;
    }

    protected function getItems(): Collection
    {
        return $this->getSearchResults()->getCollection();
    }

    protected function getTotalPages(): int
    {
        return $this->getSearchResults()->getTotalPages();
    }
}
