<?php

declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\ElasticSearch;

use Elastic\EnterpriseSearch\AppSearch\Request\Search;
use Elastic\EnterpriseSearch\AppSearch\Schema\PaginationResponseObject;
use Elastic\EnterpriseSearch\AppSearch\Schema\SearchRequestParams;
use Elastic\EnterpriseSearch\AppSearch\Schema\SimpleObject;
use Elastic\EnterpriseSearch\Client;
use Elastic\EnterpriseSearch\Response\Response;
use Illuminate\Support\Collection;

final class ElasticSearchClient
{
    private static Client $client;

    public static function getInstance(bool $forceNewInstance = false): Client
    {
        if ($forceNewInstance || !isset(self::$client)) {
            $options = [
                'host'       => config('elastic-app-search.server'),
                'app-search' => ['token' => config('elastic-app-search.private_token')],
            ];
            $client  = config('elastic-app-search.client');
            if ($client !== null) {
                $options['client'] = $client;
            }
            self::$client = new Client($options);
        }
        return self::$client;
    }

    public static function getEngineName(string $index): string
    {
        return config('elastic-app-search.engine_prefix') . $index;
    }

    public static function search(string $indexName, SearchRequestParams $searchRequestParams): Response
    {
        return self::getInstance()->appSearch()->search(new Search(ElasticSearchClient::getEngineName($indexName), $searchRequestParams));
    }

    public static function simplifiedSearch(
        string       $indexName,
        string       $searchText,
        int          $page = 1,
        int          $pageSize = 12,
        SimpleObject $filters = null,
        callable     $parseResult = null,
    ): SearchResults {
        $searchRequestParams          = new SearchRequestParams($searchText);
        $pageObject                   = new PaginationResponseObject();
        $pageObject->current          = $page;
        $pageObject->size             = $pageSize;
        $searchRequestParams->page    = $pageObject;
        if ($filters !== null) {
            $searchRequestParams->filters = $filters;
        }
        $results                      = self::search($indexName, $searchRequestParams);
        $items                        = new Collection($results->asArray()['results']);
        if ($parseResult !== null) {
            $items = $items->map($parseResult);
        }
        return new SearchResults($items, (int) $results->asArray()['meta']['page']['total_pages']);
    }
}
