<?php

declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\ElasticSearch;

use BeTo\Laravel\Exceptions\Handler;
use BeTo\Laravel\Exceptions\ProgrammingException;
use BeTo\LaravelElasticAppSearch\Interfaces\AppSearchEntityInterface;
use BeTo\LaravelElasticAppSearch\Services\GetAppSearchModels;
use Elastic\EnterpriseSearch\AppSearch\Endpoints as AppEndpoints;
use Elastic\EnterpriseSearch\AppSearch\Request\DeleteDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\IndexDocuments;
use Elastic\EnterpriseSearch\AppSearch\Request\ListDocuments;
use Elastic\EnterpriseSearch\AppSearch\Schema\Document;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class IndexEntities
{
    protected static self  $instance;
    protected ?ProgressBar $progressBar                     = null;
    protected array        $entitiesToIndexAfterTransaction = [];

    final protected function __construct(
        protected readonly AppEndpoints     $appSearchEndpoints,
        protected readonly ?OutputInterface $output,
    ) {
    }

    public static function getInstance(OutputInterface $output = null, bool $recreate = false): static
    {
        if (!isset(self::$instance) || $recreate || self::$instance->output !== $output) {
            self::$instance = new static(ElasticSearchClient::getInstance()->appSearch(), $output);
        }
        return self::$instance;
    }

    public function scheduleIndexingAfterTransaction(AppSearchEntityInterface ...$entities): void
    {
//        if (Db::transactionLevel() === 0) {
//            $this->indexEntities($entities);
//            return;
//        }
        $addEvent                              = empty($this->entitiesToIndexAfterTransaction);
        $this->entitiesToIndexAfterTransaction = array_merge($this->entitiesToIndexAfterTransaction, $entities);
        if ($addEvent) {
            DB::afterCommit(function () {
                $this->indexEntities($this->entitiesToIndexAfterTransaction);
                $this->entitiesToIndexAfterTransaction = [];
            });
        }
    }

    public function deleteAll(string $classFQN = null): void
    {
        if ($classFQN === null) {
            $indexNames = GetAppSearchModels::getEngineNames();
        } elseif (!is_a($classFQN, AppSearchEntityInterface::class, true)) {
            Handler::reportBackground(new ProgrammingException('Cannot index entities that do not implement the ' . AppSearchEntityInterface::class . ' interface'));
            return;
        } else {
            $indexNames = [$classFQN::getEngineName()];
        }
        foreach ($indexNames as $indexName) {
            $this->output?->writeln('Deleting items in <comment>'.ElasticSearchClient::getEngineName($indexName).'</comment>');
            $currentPage = 1;
            $request     = new ListDocuments(ElasticSearchClient::getEngineName($indexName));
            $request->setPageSize(100);
            $response  = $this->appSearchEndpoints->listDocuments($request);
            $pageData  = $response['meta']['page'];
            $documents = array_column($response->asArray()['results'], 'id');
            if (empty($documents)) {
                continue;
            }
            $this->progressBar = $this->output !== null ? new ProgressBar($this->output, $pageData['total_results']) : null;
            $this->progressBar?->start();
            $this->appSearchEndpoints->deleteDocuments(new DeleteDocuments(ElasticSearchClient::getEngineName($indexName), $documents));
            $this->progressBar?->advance(count($documents));
            while ($pageData['current'] < $pageData['total_pages']) {
                $request->setCurrentPage(++$currentPage);
                $response  = $this->appSearchEndpoints->listDocuments($request);
                $pageData  = $response['meta']['page'];
                $documents = array_column($response->asArray()['results'], 'id');
                $this->appSearchEndpoints->deleteDocuments(new DeleteDocuments(ElasticSearchClient::getEngineName($indexName), $documents));
                $this->progressBar?->advance(count($documents));
            }
            $this->progressBar?->finish();
            $this->output?->writeln('');
        }
    }

    public function indexAll(string $classFQN = null): void
    {
        if (!config('beto.app_search.enabled', false)) {
            return;
        }
        if ($classFQN === null) {
            $entities = GetAppSearchModels::getEntities();
        } elseif (!is_a($classFQN, AppSearchEntityInterface::class, true)) {
            Handler::reportBackground(new ProgrammingException('Cannot index entities that do not implement the ' . AppSearchEntityInterface::class . ' interface'));
            return;
        } else {
            $entities = $classFQN::getIndexableEntities();
        }
        $this->output?->writeln('<info>Indexing items</info>');
        $this->indexEntities($entities);
    }

    /**
     * @param iterable<AppSearchEntityInterface> $entities
     */
    public function indexEntities(iterable $entities): void
    {
        if (!config('beto.app_search.enabled', false)) {
            return;
        }
        $max               = is_countable($entities) ? count($entities) : 0;
        $this->progressBar = $this->output !== null ? new ProgressBar($this->output, $max) : null;
        $this->progressBar?->start();
        foreach ($this->getChunks($entities) as $indexName => $chunk) {
            $this->doIndex($indexName, ...$chunk);
        }
        $this->progressBar?->finish();
        $this->output?->writeln('');
    }

    /**
     * @param iterable<AppSearchEntityInterface> $entities
     * @return \Generator<string, array<string, Document>>
     */
    protected function getChunks(iterable $entities): \Generator
    {
        $chunks = [];
        foreach ($entities as $entity) {
            if (!$entity instanceof AppSearchEntityInterface) {
                Handler::reportBackground(new ProgrammingException('The entity provided is not an instance of ' . AppSearchEntityInterface::class));
                continue;
            }
            $chunks[$entity::getEngineName()][] = $entity->getElasticData();
            if (count($chunks[$entity::getEngineName()]) === 100) {
                yield $entity::getEngineName() => $chunks[$entity::getEngineName()];
                $chunks[$entity::getEngineName()] = [];
            }
            $this->progressBar?->advance();
        }
        foreach ($chunks as $indexName => $chunk) {
            if (empty($chunk)) {
                continue;
            }
            yield $indexName => $chunk;
        }
    }

    public function indexDocuments(string $indexName, Document ...$documents): void
    {
        if (!config('beto.app_search.enabled', false)) {
            return;
        }
        $this->progressBar = $this->output !== null ? new ProgressBar($this->output, count($documents)) : null;
        $this->progressBar?->start();
        foreach (array_chunk($documents, 100) as $chunk) {
            $this->doIndex($indexName, ...$chunk);
            $this->progressBar?->advance(count($chunk));
        }
        $this->progressBar?->finish();
        $this->output?->writeln('');
    }

    private function doIndex(string $indexName, Document ...$documents): void
    {
        if (count($documents) > 100) {
            throw new ProgrammingException('Cannot index more than 100 documents at a time. Try using array_chunk.');
        }
        if (empty($documents)) {
            throw new ProgrammingException('Cannot index no items');
        }
        $this->appSearchEndpoints->indexDocuments(new IndexDocuments(ElasticSearchClient::getEngineName($indexName), $documents));
    }
}
