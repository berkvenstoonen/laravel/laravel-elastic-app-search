<?php

declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\ElasticSearch;

use BeTo\Laravel\Exceptions\Handler;
use BeTo\Laravel\Exceptions\ProgrammingException;
use BeTo\LaravelElasticAppSearch\Interfaces\AppSearchEntityInterface;
use BeTo\LaravelElasticAppSearch\Services\GetAppSearchModels;
use Elastic\EnterpriseSearch\AppSearch\Endpoints as AppEndpoints;
use Elastic\EnterpriseSearch\AppSearch\Request\DeleteDocuments;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteEntities
{
    protected ?ProgressBar $progressBar                      = null;
    protected array        $entitiesToDeleteAfterTransaction = [];

    final protected function __construct(
        protected readonly AppEndpoints     $appSearchEndpoints,
        protected readonly ?OutputInterface $output,
    ) {
    }

    public static function getInstance(OutputInterface $output = null): static
    {
        return new static(ElasticSearchClient::getInstance()->appSearch(), $output);
    }

    public function scheduleDeletionAfterTransaction(AppSearchEntityInterface ...$entities): void
    {
        $addEvent = empty($this->entitiesToDeleteAfterTransaction);
        $this->entitiesToDeleteAfterTransaction = array_merge($this->entitiesToDeleteAfterTransaction, $entities);
        if ($addEvent) {
            DB::afterCommit(function () {
                $this->deleteEntities($this->entitiesToDeleteAfterTransaction);
            });
        }
    }

    public function deleteAll(string $classFQN = null): void
    {
        if (!config('beto.app_search.enabled', false)) {
            return;
        }
        if ($classFQN === null) {
            $entities = GetAppSearchModels::getEntities();
        } elseif (!is_a($classFQN, AppSearchEntityInterface::class, true)) {
            Handler::reportBackground(new ProgrammingException('Cannot delete entities that do not implement the ' . AppSearchEntityInterface::class . ' interface'));
            return;
        } else {
            $entities = $classFQN::getIndexableEntities();
        }
        $this->deleteEntities($entities);
    }

    /**
     * @param iterable<AppSearchEntityInterface> $entities
     * @return void
     */
    public function deleteEntities(iterable $entities): void
    {
        if (!config('beto.app_search.enabled', false)) {
            return;
        }
        $max               = is_countable($entities) ? count($entities) : 0;
        $this->progressBar = $this->output !== null ? new ProgressBar($this->output, $max) : null;
        $this->progressBar?->start();
        foreach ($this->getChunks($entities) as $indexName => $chunk) {
            $this->doDelete($indexName, ...$chunk);
        }
        $this->progressBar?->finish();
        $this->output?->writeln('');
    }

    /**
     * @param iterable<AppSearchEntityInterface> $entities
     * @return \Generator<string, array<int, string>>
     */
    protected function getChunks(iterable $entities): \Generator
    {
        $chunks = [];
        foreach ($entities as $entity) {
            if (!$entity instanceof AppSearchEntityInterface) {
                throw new ProgrammingException('The entity provided is not an instance of ' . AppSearchEntityInterface::class);
            }
            $chunks[$entity::getEngineName()][] = $entity->getAppSearchId();
            if (count($chunks[$entity::getEngineName()]) === 100) {
                yield $entity::getEngineName() => $chunks[$entity::getEngineName()];
                $chunks[$entity::getEngineName()] = [];
            }
            $this->progressBar?->advance();
        }
        foreach ($chunks as $indexName => $chunk) {
            yield $indexName => $chunk;
        }
    }

    /**
     * @param iterable<string> $ids
     */
    public function deleteIds(string $indexName, iterable $ids): void
    {
        if (!config('beto.app_search.enabled', false)) {
            return;
        }
        $max               = is_countable($ids) ? count($ids) : 0;
        $this->progressBar = $this->output !== null ? new ProgressBar($this->output, $max) : null;
        $this->progressBar?->start();
        $chunk      = [];
        $chunkCount = 0;
        foreach ($ids as $id) {
            $chunk[] = $id;
            $chunkCount++;
            if ($chunkCount === 100) {
                $this->doDelete($indexName, ...$chunk);
                $chunkCount = 0;
                $chunk      = [];
            }
            $this->progressBar?->advance();
        }
        $this->progressBar?->finish();
        $this->output?->writeln('');
    }

    protected function doDelete(string $indexName, string ...$ids): void
    {
        $this->appSearchEndpoints->deleteDocuments(new DeleteDocuments(ElasticSearchClient::getEngineName($indexName), $ids));
    }
}
