<?php

declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\ElasticSearch;

use Illuminate\Support\Collection;

final class SearchResults
{
    public function __construct(
        /** @var Collection<mixed> $collection */
        private readonly Collection $collection,
        private readonly int        $totalPages,
    ) {
    }

    public function getCollection(): Collection
    {
        return $this->collection;
    }

    public function getTotalPages(): int
    {
        return $this->totalPages;
    }
}
