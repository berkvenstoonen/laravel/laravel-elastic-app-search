<?php
declare(strict_types=1);

namespace BeTo\LaravelElasticAppSearch\Interfaces;

use Elastic\EnterpriseSearch\AppSearch\Schema\Document;

interface AppSearchEntityInterface
{
    /**
     * @return iterable<AppSearchEntityInterface>
     */
    public static function getIndexableEntities(): iterable;

    public static function getEngineName(): string;

    public function getAppSearchId(): string;

    public function getElasticData(): Document;
}
