<?php

namespace BeTo\LaravelElasticAppSearch;

use BeTo\Laravel\Exceptions\DependencyException;
use BeTo\LaravelElasticAppSearch\Commands\Index;
use BeTo\LaravelElasticAppSearch\ElasticSearch\DeleteEntities;
use BeTo\LaravelElasticAppSearch\ElasticSearch\IndexEntities;
use BeTo\LaravelElasticAppSearch\Http\Livewire\SearchForm;
use BeTo\LaravelElasticAppSearch\Interfaces\AppSearchEntityInterface;
use BeTo\LaravelElasticAppSearch\Services\GetAppSearchModels;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

final class SearchServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->commands([Index::class]);
        $this->mergeConfigFrom(__DIR__ . '/../config/elastic-app-search.php', 'elastic-app-search');
    }

    /**
     * Bootstrap services.
     * @throws DependencyException
     */
    public function boot(): void
    {
        $this->initLiveWireComponents();
        $this->initViewComponents();
        $this->publishes([__DIR__ . '/../config/elastic-app-search.php' => config_path('elastic-app-search.php')], 'config');
        if (config('beto.app_search.enabled', false)) {
            $this->registerEntityEvents();
        }
    }

    private function initLiveWireComponents(): void
    {
        Livewire::component('beto-search-form', SearchForm::class);
    }

    private function initViewComponents(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'beto');
        $this->publishes([__DIR__ . '/../resources/views' => resource_path('views/vendor/beto')]);
    }

    /**
     * @throws DependencyException
     */
    private function registerEntityEvents(): void
    {
        foreach (GetAppSearchModels::getClasses() as $className) {
            if (!is_a($className, Model::class, true)) {
                continue;
            }
            $className::saved(function (AppSearchEntityInterface $item) {
                IndexEntities::getInstance()->scheduleIndexingAfterTransaction($item); // TODO @JB this needs to be disabled for he force sync
            });
            $className::deleted(function (AppSearchEntityInterface $item) {
                DeleteEntities::getInstance()->scheduleDeletionAfterTransaction($item);
            });
        }
    }
}
