<?php

namespace BeTo\LaravelElasticAppSearch\Commands;

use BeTo\LaravelElasticAppSearch\ElasticSearch\ElasticSearchClient;
use BeTo\LaravelElasticAppSearch\ElasticSearch\IndexEntities;
use Elastic\EnterpriseSearch\AppSearch\Request\DeleteDocuments;
use Illuminate\Console\Command;
use Webmozart\Assert\Assert;

final class Index extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'content:index {--c|class=} {id?}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index all models with the AppSearchEntity Interface';

    public function handle(): int
    {
        $class = $this->option('class');
        $id = $this->argument('id');
        $indexer = IndexEntities::getInstance($this->output);
        Assert::nullOrClassExists($class);
        if ($class !== null && $id !== null) {
            /** @noinspection PhpUndefinedMethodInspection */
            $item = $class::findOrFail($id);
            $indexer->indexEntities([$item]);
            return 0;
        }
        $indexer->deleteAll($class);
        $indexer->indexAll($class);
        return 0;
    }
}
