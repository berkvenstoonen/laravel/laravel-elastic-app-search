<?php
declare(strict_types=1);

use GuzzleHttp\Client;

return [
    'server'          => env('ELASTIC_APP_SEARCH_SERVER', 'https://example.com:3002'),
    'private_token'   => env('ELASTIC_APP_SEARCH_PRIVATE_TOKEN'),
    'search_token'    => env('ELASTIC_APP_SEARCH_SEARCH_TOKEN'),
    'client'          => app()->runningInConsole() ? null : new Client(),
    'engine_prefix'   => env('APP_ENV') . '-' . env('ELASTIC_APP_SEARCH_INDEX_PREFIX', env('APP_NAME')) . '-',
    'model_namespace' => 'App\Models',
];
