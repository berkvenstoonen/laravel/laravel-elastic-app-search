<div>
    <div class="dynamic-grid max-w-screen-3xl mx-auto loader-container" wire:loading.grid>
        @for($i = 0; $i < 12; $i++)
            <div class="py-6 px-4 mx-auto">
                <!--suppress CheckImageSize -->
                <img src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="rounded-lg item-loader" height="600" width="600" alt="@lang('common.loader')"/>
            </div>
        @endfor
    </div>
    @if(!$loaded)
        <div class="mt-4 h-1" wire:loading.remove x-data="loadMoreBlock" x-init="checkToLoad()"></div>
    @else
        <div class="dynamic-grid max-w-screen-3xl mx-auto">
            @foreach($items as $item)
                <x-item.tile :item="$item" :wire:key="$item->id" />
            @endforeach
        </div>
        @if($totalPages > $page)
            <livewire:item.load-more-block :page="$page + 1" :filters="$filters" />
        @else
            <div x-init="noMore()"></div>
        @endif
    @endif
</div>
