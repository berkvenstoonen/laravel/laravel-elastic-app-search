<form id="search-form" class="relative mx-auto text-gray-600 w-full grid items-center" action="{{ route($route) }}" autocomplete="off">
    <label for="search" class="sr-only">@lang('common.actions.search')</label>
    <div wire:ignore>
        <x-beto-form.select2
            id="search"
            model="search"
            :tags="$tags"
            :selected="$selected"
            :autocompleteUrl="$autocompleteUrl"
            :autocompleteHeaders="$autocompleteHeaders"
            :autocompleteFilters="$autocompleteFilters"
        >
            <x-slot name="labelHtml"></x-slot>
        </x-beto-form.select2>
    </div>

    <button wire:click.prevent="" type="submit" class="absolute right-4" aria-label="{{ trans('common.actions.search') }}" style="top: 36px">
        <svg xmlns="http://www.w3.org/2000/svg" class="text-gray-600 h-4 w-4 fill-current" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"/>
        </svg>
    </button>
    <script type="javascript">

    </script>
</form>
