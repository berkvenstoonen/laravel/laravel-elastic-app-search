import {betoSelect2} from "../../../laravel/resources/js/select2";
import jQuery from 'jquery';

(factory => {
    factory(jQuery);
})($ => {
    betoSelect2.updateOptionsHook(($element, options) => {
        const autocompleteUrl = $element.data('autocompleteUrl');
        if (!autocompleteUrl) {
            return;
        }
        const emptyUrl = $element.data('emptyUrl');
        const minAutocompleteLength = $element.data('minAutocompleteLength');
        const autocompleteHeaders = $element.data('autocompleteHeaders');
        const autocompleteFilters = $element.data('autocompleteFilters');
        options.ajax = {
            url: params => params.term !== undefined && params.term.trim().length >= minAutocompleteLength ? autocompleteUrl : emptyUrl,
            headers: autocompleteHeaders,
            dataType: 'json',
            type: 'POST',
            data: params => (JSON.stringify({query: params.term, filters: autocompleteFilters})),
            processResults: data => {
                const teams = [];
                const dataPoints = [];
                data.results.forEach(result => {
                    if (result._meta.engine.endsWith('data-point-values')) {
                        const dataPoint = {id: result._meta.id, text: result.autocomplete.raw};
                        if (result.autocomplete.snippet !== null) {
                            dataPoint.html = result.autocomplete.snippet;
                        }
                        dataPoints.push(dataPoint);
                    } else {
                        const team = {id: result._meta.id, text: result.autocomplete.raw};
                        if (result.autocomplete.snippet !== null) {
                            team.html = result.autocomplete.snippet;
                        }
                        teams.push(team);
                    }
                });
                const results = [];
                if (dataPoints.length > 0) {
                    results.push({
                        text: 'Tags',
                        children: dataPoints
                    });
                }
                if (teams.length > 0) {
                    results.push({
                        text: 'Shared With',
                        children: teams
                    });
                }
                return {results};
            },
        };
    });
});
